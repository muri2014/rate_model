#! plots the trajectory according to 'N_points' sets of initial conditions that are generated randomly and the parameters of the model selected. TEst
#! /usr/bin/env python
from scipy.integrate import ode
import numpy as np
from pylab import *
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# Notes: Simply change the coupling matrix gf,gb,gs to see the trajectories in the s_i space.
# Use tstop  = 10.0; dt = .01; N_points=10; t_vis = 0.1; 

# close previous figures
plt.close("all")


ion()

N_points =10.; #define the number of initial conditions to test

# initial conditions
Si = np.random.uniform(0,0.0001,size=[N_points,3]);
Ri = np.random.uniform(0,0.0001,size=[N_points,3]);

x0 = np.array([Si[:,0],Ri[:,0],Si[:,1],Ri[:,1],Si[:,2],Ri[:,2]]).T


# model parameters:
Idc = 0.25; #Input current
I0 = 0.0439; #nA
Iti = Idc - I0;

tau = 20./1000; #constant time in s
Smax = 0.045;
kappa = .5;
alfa = 0.564; #power
X0 = .185; #Amplitude
X0t = (1-np.exp(-.695/tau))*X0; # aprox 2.57e-3

# couplings 
weight = 20.; #weight to the coupling matrix

# Winner take all: 20/20/10. Learning
gf = 30. #forward
gb = 1. #backwards
gs = 0. #self connection

# WLC: 20/9/10. Recall
#gf = 20. #forward
#gb = 9. #backwards
#gs = 10. #self connection

G = weight*np.array([ [gs, gf, gb], [gb, gs, gf], [ gf, gb, gs]]) #coupling matrix

# integration time
tstop  = 10.0; #time stop
dt = .01 #time step

# Time at which I start plotting the trajectories. It does not affect computation. Only for visualization purposes. Wait until the trajectory reaches the dynamics of the model.
t_vis = 0.1; 

# define function to compute max(x,0)
def max_0(x):
    #return x+1./1*np.log(x+np.exp(-1*x)) #this is continuos 
    return np.maximum(x,0)

# function to compute []**alfa, which is rate/X0 
# the input variable Si has to be a [s1,s2,s3]
def frate(Si):
    return (max_0 ( Iti-np.dot(G,Si) ) )**alfa

# set of differential equations
def func(t,y):
    dydt = [None]*6

    x = frate([y[0],y[2],y[4]]) #input s1,s2,s3
    
    dydt[0]= ((y[1]-kappa*y[0]) * ((Smax-y[0])/Smax))/tau; #d(s1)/dt
    dydt[1]= X0t*x[0] - y[1]/tau; #d(r1)/dt
    
    dydt[2]=((y[3]-kappa*y[2])*((Smax-y[2])/Smax))/tau; #d(s2)/dt
    dydt[3]= X0t*x[1] - y[3]/tau; #d(r2)/dt
    
    dydt[4]=((y[5]-kappa*y[4])*((Smax-y[4])/Smax))/tau;  #d(s3)/dt
    dydt[5]= X0t*x[2] - y[5]/tau; #d(r3)/dt

    return dydt

# Integrate equations
yam = ode(func).set_integrator('vode', with_jacobian=False)


yam.set_initial_value(x0[0,:]) #Integration for the first set of init conditions
y=[]
t=[]
while yam.successful() and yam.t < tstop:
    yam.integrate(yam.t+dt)
    y.append(yam.y)
    t.append(yam.t)
# the variables s1,r1,..,s3,r3 are in the array y[]. Time t[]
y=np.array(y)

y_acc = np.zeros((x0.shape[0],size(t),x0.shape[1])) #Init for the rest of init conditions
y_acc [0,:,:]=y; #Copy the results of the 1st ite in y_acc

for count in range(1,x0.shape[0]): #For the rest of init conditions
	print(count)
	yam.set_initial_value(x0[count,:])
	y=[]
	t=[]
	while yam.successful() and yam.t < tstop:
		yam.integrate(yam.t+dt)
		y.append(yam.y)
		t.append(yam.t)
	# the variables s1,r1,..,s3,r3 are in the array y[]. Time t[]
	y=np.array(y)
	y_acc [count,:,:] = y;

# Estimate rate of neurons
rate=[]; #ini array

for i in range(0,size(t)):
	rate.append(1000*X0*frate(y[i,[0,2,4]]))
rate = np.array(rate)


# Plot s_i trajectories
t_red = np.arange(np.round(t_vis/dt),size(t),1); # Plot only the last part of the trajectories. Starting from t_vis
t_red=t_red.astype(int)

mpl.rcParams['legend.fontsize'] = 10
fig = plt.figure()
ax = fig.gca(projection='3d')
for count in range(0,int(N_points)):
	print(count)
	if count==N_points-1: # color of last line different
		color_line = 'k'
	else:
		color_line = 'r' 
	#ax.plot(y_acc[count,:,0], y_acc[count,:,2], y_acc[count,:,4], color=color_line, label='Trajectories s_i')
	ax.plot(y_acc[count,t_red,0], y_acc[count,t_red,2], y_acc[count,t_red,4], color=color_line, label='Trajectories s_i')
	ax.scatter(y_acc[count,size(t)-1,0], y_acc[count,size(t)-1,2], y_acc[count,size(t)-1,4]) # Remark the last point of the trajectory
#ax.plot(y_acc[1,:,0], y_acc[1,:,2], y_acc[1,:,4], label='Trajectories s_i')
#ax.plot(y_acc[2,:,0], y_acc[2,:,2], y_acc[2,:,4], label='Trajectories s_i')
ax.set_xlabel('S_1')
ax.set_ylabel('S_2')
ax.set_zlabel('S_3')
ax.legend()
plt.show()


# Final points of the s_i trajectories:
#(y_acc[count,size(t)-1,0], y_acc[count,size(t)-1,2], y_acc[count,size(t)-1,4])

np.vstack((y_acc[:,size(t)-1,0],y_acc[:,size(t)-1,2],y_acc[:,size(t)-1,4])).T


## Plot s_i(t)
#figure()
#title('s')
#plot(t,y[:,0],label='s_1')
#plot(t,y[:,2],label='s_2')
#plot(t,y[:,4],label='s_3')
#legend()

## Plot r_i(t)
#figure()
#title('r')
#plot(t,y[:,1],label='y_1')
#plot(t,y[:,3],label='y_2')
#plot(t,y[:,5],label='y_3')
#legend()

# Plot rate(t). Corresponds to the last trajectory (the one in black)
figure()
title ('rate')
plot(t,rate[:,0],label='y_1')
plot(t,rate[:,1],label='y_2')
plot(t,rate[:,2],label='y_3')
legend()

