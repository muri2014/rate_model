#! /usr/bin/env python
from scipy.integrate import ode
import numpy as np
from pylab import *

# close previous figures
plt.close("all")


ion()

# initial conditions
s1 = 0.01;
s2 = 0.0001;
s3 = 0.0099;
r1 = 0.01;
r2 = 0.0001;
r3 = 0.0099;

x0=np.array([s1, r1, s2, r2, s3, s3 ]);

# model parameters:
Idc = .25; #Input current
I0 = 0.0439; #nA
Iti = Idc - I0;

tau = 50./1000; #constant time in s
Smax = 0.045;
kappa = .5;
alfa = 0.564; #power
X0 = .185; #Amplitude
X0t = (1-np.exp(-.695/50.))*X0; # aprox 2.57e-3

# couplings 
g_11 = 0.; #no self connections
g_22 = 0.;
g_33 = 0.;
g_12 = 1.5; 
g_13 = 1.5;
g_21 = 1.5;
g_23 = 1.5;
g_31 = 1.5;
g_32 = 1.5;
weight = 20.; #weight to the coupling matrix
G = weight*np.array([ [g_11, g_12, g_13], [g_21, g_22, g_23], [ g_31, g_32, g_33]]) #

# integration time
tstop  = 10.0; #time stop
dt = .01 #time step


# define function to compute max(x,0)
def max_0(x):
    #return x+1./1*np.log(x+np.exp(-1*x)) #this is continuos 
    return np.maximum(x,0)

# function to compute []**alfa, which is rate/X0 
def frate(y):
    return (max_0 ( Iti-np.dot(G,[y[0],y[2],y[4]]) ) )**alfa

# set of differential equations
def func(t,y):
    dydt = [None]*6

    x = frate(y)
    dydt[0]= ((y[1]-kappa*y[0]) * ((Smax-y[0])/Smax))/tau; #d(s1)/dt
    dydt[1]= X0*x[0] - y[1]/tau; #d(r1)/dt
    
    dydt[2]=((y[3]-kappa*y[2])*((Smax-y[2])/Smax))/tau; #d(s2)/dt
    dydt[3]= X0*x[1] - y[3]/tau; #d(r2)/dt
    
    dydt[4]=((y[5]-kappa*y[4])*((Smax-y[4])/Smax))/tau;  #d(s3)/dt
    dydt[5]= X0*x[2] - y[5]/tau; #d(r3)/dt

    return dydt

# Integrate equations
yam = ode(func).set_integrator('vode', with_jacobian=False)
yam.set_initial_value(x0)
y=[]
t=[]
while yam.successful() and yam.t < tstop:
    yam.integrate(yam.t+dt)
    y.append(yam.y)
    t.append(yam.t)
# the variables s1,r1,..,s3,r3 are in the array y[]. Time t[]
y=np.array(y)

# Estimate rate of neurons
rate=[]; #ini array

for i in range(0,size(t)):
	rate.append(X0*frate(y[i,:]))
rate = np.array(rate)


#figure()
#plot(t,y[:,[0,1,2]])
#ylim([-1,3])
#
#figure()
#plot(t,y[:,[3,4,5]])
#ylim([-1,3])

# Plot s_i and r_i
figure()
title('s')
plot(t,y[:,0],label='s_1')
plot(t,y[:,2],label='s_2')
plot(t,y[:,4],label='s_3')
legend()

figure()
title('r')
plot(t,y[:,1],label='y_1')
plot(t,y[:,3],label='y_2')
plot(t,y[:,5],label='y_3')
legend()


figure()
plot(t,rate[:,0],label='y_1')
plot(t,rate[:,1],label='y_2')
plot(t,rate[:,2],label='y_3')

legend()


#
#gNa = 7.15e-6
#ENa = 50
#gK  = 1.43e-6
#EK  = -95
#glk = 0.021e-6
#Elk = -55
#gKlk = 0.00572e-6
#EKlk = -95
#C = .143e-9
#Istim = -2000e-9
#Erev = -80
#
#def hhfunc(t,z):
#    V,n,m,h = z[0],z[1],z[2],z[3]
#    #V,n,m,h,S,R = z[0],z[1],z[2],z[3],z[4],z[5]
#    INa = gNa*m**3*h*(V-ENa)
#    IK = gK*n**4*(V-EK)
#    Ilk = glk*(V-Elk)
#    IKlk = gKlk*(V-EKlk)
#    #Isyn = 50e-9 * S*(V-Erev)
#    dVdt = (-INa-IK-Ilk-IKlk-Istim)/C #- Isyn/C
#    dndt = .032*(-50-V)/(np.exp((-50-V)/5)-1)   *(1-n) - 0.5*np.exp((-55-V)/40)             *n
#    dmdt = .32*(-52-V)/(np.exp((-52-V)/4)-1)   *(1-m) - 0.28*(25+V)/(np.exp((25+V)/5)-1)   *m
#    dhdt = .128*np.exp((-48-V)/18)             *(1-h) - 4./(np.exp((-25-V)/5)+1)           *h
#    #dSdt = (R- kappa*S) * (Smax-S)/Smax
#    #dRdt = heaviside_1D(V + 20e-3) - R
#    return [dVdt,dndt,dmdt,dhdt]#,dSdt,dRdt]
#
#yam = ode(hhfunc).set_integrator('vode', with_jacobian=False)
#yam.set_initial_value([-0.06,0.0529324,0.3176767,0.5961207])
#y=[]
#t=[]
#while yam.successful() and yam.t < tstop:
#    yam.integrate(yam.t+dt)
#    y.append(yam.y)
#    t.append(yam.t)
#
#
#figure()
#y=np.array(y)
#plot(t,y)
#show()
#
#
