#! plots the trajectory and variables in time according to one set of initial conditions and the parameters of the model selected.
#! /usr/bin/env python
from scipy.integrate import ode
import numpy as np
from pylab import *
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# close previous figures
plt.close("all")


ion()

# initial conditions
s1 = 0.01;
s2 = 0.0001;
s3 = 0.0099;
r1 = 0.01;
r2 = 0.0001;
r3 = 0.0099;

x0=np.array([s1, r1, s2, r2, s3, s3 ]);

# model parameters:
Idc = .25; #Input current
I0 = 0.0439; #nA
Iti = Idc - I0;

tau = 50./1000; #constant time in s
Smax = 0.045;
kappa = .5;
alfa = 0.564; #power
X0 = .185; #Amplitude
X0t = (1-np.exp(-.695/50.))*X0; # aprox 2.57e-3

# couplings 
g_11 = 0.; #no self connections
g_22 = 0.;
g_33 = 0.;
g_12 = 1.5; 
g_13 = 1.5;
g_21 = 1.5;
g_23 = 1.5;
g_31 = 1.5;
g_32 = 1.5;
weight = 20.; #weight to the coupling matrix
G = weight*np.array([ [g_11, g_12, g_13], [g_21, g_22, g_23], [ g_31, g_32, g_33]]) #

# integration time
tstop  = 10.0; #time stop
dt = .01 #time step


# define function to compute max(x,0)
def max_0(x):
    #return x+1./1*np.log(x+np.exp(-1*x)) #this is continuos 
    return np.maximum(x,0)

# function to compute []**alfa, which is rate/X0 
# the input variable y has to be a [s1,s2,s3]
def frate(Si):
    return (max_0 ( Iti-np.dot(G,Si) ) )**alfa

# set of differential equations
def func(t,y):
    dydt = [None]*6

    x = frate([y[0],y[2],y[4]]) #input s1,s2,s3
    
    dydt[0]= ((y[1]-kappa*y[0]) * ((Smax-y[0])/Smax))/tau; #d(s1)/dt
    dydt[1]= X0*x[0] - y[1]/tau; #d(r1)/dt
    
    dydt[2]=((y[3]-kappa*y[2])*((Smax-y[2])/Smax))/tau; #d(s2)/dt
    dydt[3]= X0*x[1] - y[3]/tau; #d(r2)/dt
    
    dydt[4]=((y[5]-kappa*y[4])*((Smax-y[4])/Smax))/tau;  #d(s3)/dt
    dydt[5]= X0*x[2] - y[5]/tau; #d(r3)/dt

    return dydt

# Integrate equations
yam = ode(func).set_integrator('vode', with_jacobian=False)
yam.set_initial_value(x0)
y=[]
t=[]
while yam.successful() and yam.t < tstop:
    yam.integrate(yam.t+dt)
    y.append(yam.y)
    t.append(yam.t)
# the variables s1,r1,..,s3,r3 are in the array y[]. Time t[]
y=np.array(y)

# Estimate rate of neurons
rate=[]; #ini array

for i in range(0,size(t)):
	rate.append(X0*frate(y[i,[0,2,4]]))
rate = np.array(rate)



# Plot s_i(t)
figure()
title('s')
plot(t,y[:,0],label='s_1')
plot(t,y[:,2],label='s_2')
plot(t,y[:,4],label='s_3')
legend()

# Plot r_i(t)
figure()
title('r')
plot(t,y[:,1],label='y_1')
plot(t,y[:,3],label='y_2')
plot(t,y[:,5],label='y_3')
legend()

# Plot rate(t)
figure()
plot(t,rate[:,0],label='y_1')
plot(t,rate[:,1],label='y_2')
plot(t,rate[:,2],label='y_3')
legend()

# Plot s_i trajectories
mpl.rcParams['legend.fontsize'] = 10
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(y[:,0], y[:,2], y[:,4], label='Trajectories s_i')
ax.set_xlabel('S_1')
ax.set_ylabel('S_2')
ax.set_zlabel('S_3')
ax.legend()
plt.show()




